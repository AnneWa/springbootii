package com.cdaJava.SpringBootII.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class GrettingsController {

    @GetMapping("/hello-world")
    @ResponseBody
    public String getGrettings(){
        return "Hello World";
    }

    @GetMapping("/hello-to")
    @ResponseBody
    public String getGrettingsTo(@RequestParam(required=false, defaultValue = "Simplon") String name ){
        return "Hello " + name;
    }
}
